package test;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
class Account{
	protected int accNumber;
	protected String name;
	protected String email;
	protected int phone;
	protected float balance;
	
	public Account() {
		
	}

	public Account(int accNumber, String name, String email, int phone, float balance) {
		this.accNumber = accNumber;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.balance = balance;
	}
	public void setAccNumber(int accNumber)
	{
		this.accNumber=accNumber;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public void setPhone(int phone)
	{
		this.phone = phone;
	}
	public void setBalance(float balance)
	{
		this.balance= balance;
	}
	public int getAccNumber()
	{
		return this.getAccNumber();
	}
	public String getName()
	{
		return this.name;
	}
	public String getEmail()
	{
		return this.email;
	}
	public int getPhone()
	{
		return this.phone;
	}
	public float getBalance()
	{
		return this.balance;
	}
	@Override
	public String toString() {
		return String.format("%-10s%-10s%-10s%-15d%-10f",this.accNumber,this.name,this.email,this.phone,this.balance);
	}
	@Override
	public boolean equals(Object obj) {
		Account a = (Account)obj;
		if(this.accNumber==a.accNumber)
		{
			return true;
		}
		return false;
		
	}
}


public class Program {
	public static String getLine()
	{
		try {
			BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
			return read.readLine();
		}
		catch(IOException e)
		{
			throw new RuntimeException();
		}
	}
	public static void addAccount(List<Account> list)
	{
		Account a = new Account();
		System.out.print("Add Account Number	:	");
		a.setAccNumber(Integer.parseInt(Program.getLine()));
		System.out.print("Add Name	:	");
		a.setName(Program.getLine());
		System.out.print("Add Email	:	");
		a.setEmail(Program.getLine());
		System.out.print("Add Phone Number	:	");
		a.setPhone(Integer.parseInt(Program.getLine()));
		System.out.print("Add Balance	:	");
		a.setBalance(Float.parseFloat(Program.getLine()));
		list.add(a);
	}
	private static void removeAccount(List<Account> list) 
	{
		Account ac = new Account();
		System.out.println("Enter Account Number	:	"	);
		ac.accNumber=Integer.parseInt(Program.getLine());
			if(list.contains(ac))
			{
				list.remove(ac);
			}	
	}
	private static void withdraw(List<Account> list)
	{
		Account ac = new Account();
		float amount=0;
		int i;
		System.out.println("Enter Account Number	:	"	);
		ac.accNumber=Integer.parseInt(Program.getLine());
			if(list.contains(ac))
			{
				i=list.indexOf(ac);
				ac = list.get(i);
				System.out.print("Enter Amount to withdraw	:	");
				amount = Float.parseFloat(Program.getLine());
				if(ac.balance>amount)
				{
					ac.balance= ac.balance-amount;
				}
				else
				{
					System.out.println("Your account doesn't have enough amount");
				}
			}
			else
				System.out.println("Enter appropriate Account Number");
		
	}private static void deposit(List<Account> list)
	{
		Account ac = new Account();
		float amount=0;
		int i;
		System.out.println("Enter Account Number	:	"	);
		ac.accNumber=Integer.parseInt(Program.getLine());
			if(list.contains(ac))
			{
				i=list.indexOf(ac);
				ac = list.get(i);
				System.out.print("Enter Amount to Deposit	:	");
				amount = Float.parseFloat(Program.getLine());
					ac.balance= ac.balance+amount;
			}
			else
				System.out.println("Enter appropriate Account Number");
		
	}
	public static void transfer(List<Account> list)
	{
		Program.withdraw(list);
		Program.deposit(list);
	}

	public static void main(String[] args) {
		//Account ac = new Account();
		//System.out.println(ac.toString());
		List<Account>list = new ArrayList<>();
		Program.addAccount(list);
		Program.addAccount(list);

		for(Account a : list)
		System.out.println(a.toString());
		
		//Program.removeAccount(list);
		
		//for(Account a : list)
			//System.out.println(a.toString());
		Program.withdraw(list);
		for(Account a : list)
			System.out.println(a.toString());
		Program.deposit(list);
		for(Account a : list)
			System.out.println(a.toString());
		Program.transfer(list);
		for(Account a : list)
			System.out.println(a.toString());
	}
	
}
