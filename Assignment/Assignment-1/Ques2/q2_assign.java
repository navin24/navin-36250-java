class q2_assign{
    public static void main(String args[])
    {
        int num=(int)9348.39f;
        long a = (long)100;
        short b = (short)80999;
        long c = (long)2373467e18;
        byte d = (byte)129;
        float f = (float)218.928;
        double g = 2930.3f;
        char h = (char)-3 ;
        boolean i = false;

        System.out.println("Number: "+num);
        System.out.println("size of int data type is: "+(Integer.SIZE/8)+" bytes");
        System.out.println("Number: "+a);
        System.out.println("size of Long Int data type is: "+(Long.SIZE/8)+" bytes");
        System.out.println("Number: "+b);
        System.out.println("size of short data type is: "+(Short.SIZE/8)+" bytes");
        System.out.println("Number: "+d);
        System.out.println("size of byte data type is: "+(Byte.SIZE/8)+" bytes");
        System.out.println("Number: "+f);
        System.out.println("size of float data type is: "+(Float.SIZE/8)+" bytes");
        System.out.println("Number: "+g);
        System.out.println("size of double data type is: "+(Double.SIZE/8)+" bytes");
        System.out.println("Char: "+h);
        System.out.println("size of char data type is: "+(Character.SIZE/8)+" bytes");
        System.out.println("Boolean Value:" +i);
    }
}
