import java.io.Console;

class q3_a_assign{
    public static void main(String arsgs[])
    {
        Console c = System.console();
        int sum=0,rem;
        System.out.print("Enter a number: ");
        int num=Integer.parseInt(c.readLine());
        while(num!=0)
        {
            rem=num%10;
            num=num/10;
            sum=sum+rem;
        }
        System.out.println("Sum of digits: "+sum);
    }
}