package test;

public class SalariedEmployee extends Employee{
	private double weeklySalary;
	public SalariedEmployee() {
		this.weeklySalary = 0;
	}
	
	public SalariedEmployee(double weeklySalary) {
		super();
		this.weeklySalary = weeklySalary;
	}
	
	public double getWeeklySalary() {
		return weeklySalary;
	}

	public void setWeeklySalary(double weeklySalary) {
		this.weeklySalary = weeklySalary;
	}

	public void printPayrollSalaried() {
		System.out.println();
		System.out.println("Name : "+this.getFirstName()+" "+this.getLastName());
		System.out.println("Type : Salaried");
		System.out.println("Weekly salary : "+this.weeklySalary);
		System.out.println();
	}
	
	public void payrollCalculation() {
		this.printPayrollSalaried();
	}
	
}
