package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class EmployeeFactory{
	public static Employee getInstance( int choice ) {
		Employee emp = null;
		switch( choice ) {
		case 0:
			break;
			
		case 1:
			emp = new SalariedEmployee();
			break;
			
		case 2:
			emp = new HourlyEmployee();
			break;
			
		case 3:
			emp = new CommissionRateEmployee();
			break;
			
		case 4:
			emp = new BasePlusCommisionEmployee();
			break;
			
			default :
				System.out.println("Wrong choice");
				break;
		}
		return emp;
	}
}
public class Program {
	
	public static String getLine() {
		try {
			BufferedReader read = new BufferedReader( new InputStreamReader(System.in ) );
			return read.readLine();
		}
		catch( IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void acceptRecord(Employee emp) {
		if( emp instanceof SalariedEmployee ) {
			SalariedEmployee sal = ( SalariedEmployee )emp;
			
			System.out.println("Name : ");
			sal.setFirstName(Program.getLine());
			System.out.println("Surname : ");
			sal.setLastName(Program.getLine());
			
			System.out.println("Enter weekly salary : ");
			sal.setWeeklySalary(Double.parseDouble(Program.getLine()));
		}
		else if( emp instanceof HourlyEmployee ) {
			HourlyEmployee hr = ( HourlyEmployee )emp;
			
			System.out.println("Name : ");
			hr.setFirstName(Program.getLine());
			System.out.println("Surname : ");
			hr.setLastName(Program.getLine());
			
			System.out.println("Enter Hourly wage : ");
			hr.setHourlyWage(Double.parseDouble(Program.getLine()));
			System.out.println("Enter hours worked : ");
			hr.setHoursWorked(Double.parseDouble(Program.getLine()));
		}
		else if( emp instanceof BasePlusCommisionEmployee ) {
			BasePlusCommisionEmployee bas = ( BasePlusCommisionEmployee )emp;
			
			System.out.println("Name : ");
			bas.setFirstName(Program.getLine());
			System.out.println("Surname : ");
			bas.setLastName(Program.getLine());
			
			System.out.println("Enter Gross sale : ");
			bas.setGrossSales(Double.parseDouble(Program.getLine()));
			System.out.println("Enter commission rate : ");
			bas.setCommissionRate(Double.parseDouble(Program.getLine()));
			System.out.println("Enter Base salary : ");
			bas.setBaseSalary(Double.parseDouble(Program.getLine()));
			bas.setBaseSalary( ( ((bas.getBaseSalary() * 10 ) / 100) + bas.getBaseSalary() ) );
		}
		else if( emp instanceof CommissionRateEmployee ) {
			CommissionRateEmployee com = ( CommissionRateEmployee )emp;
			
			System.out.println("Name : ");
			com.setFirstName(Program.getLine());
			System.out.println("Surname : ");
			com.setLastName(Program.getLine());
			
			System.out.println("Enter Gross sale : ");
			com.setGrossSales(Double.parseDouble(Program.getLine()));
			System.out.println("Enter commission rate : ");
			com.setCommissionRate(Double.parseDouble(Program.getLine()));
		}
	
	}
	
	private static int menuList() {
		System.out.printf("\n0.Exit\n1.Payroll calculation of salaried employee\n2.Payroll calculation of hourly employee\n3.Payroll calculation of commission employee\n4.Payroll calculation of baseplus-commission employee\nEnter your choice : \n");
		return Integer.parseInt(Program.getLine());
	}
	
	public static void main(String[] args) {
		int choice = 0;
		
		while( ( choice = Program.menuList() ) != 0 ) {
			Employee emp = null;
			emp = EmployeeFactory.getInstance( choice );
			if( emp != null ) {
				Program.acceptRecord( emp );
				emp.payrollCalculation();
			}
		}
	}
}
