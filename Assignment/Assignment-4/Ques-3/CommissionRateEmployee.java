package test;

public class CommissionRateEmployee extends Employee {
	double grossSales;
	double commissionRate;
	
	public CommissionRateEmployee() {
		this.commissionRate = 0;
		this.grossSales = 0;
	}

	public CommissionRateEmployee(double grossSales, double commissionRate) {
		super();
		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
	}

	public double getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(double grossSales) {
		this.grossSales = grossSales;
	}

	public double getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(double commissionRate) {
		this.commissionRate = commissionRate;
	}
	
	public void payrollCalculation() {
		double salary = this.commissionRate * this.grossSales;
		this.printPayrollCommission( salary );
	}

	private void printPayrollCommission(double salary) {
		System.out.println();
		System.out.println("Name : "+this.getFirstName()+" "+this.getLastName());
		System.out.println("Type : commission");
		System.out.println("Gross sales : "+this.getGrossSales());
		System.out.println("Commision rate : "+this.getCommissionRate());
		System.out.println("salary : "+salary);
		System.out.println();
	}
}
