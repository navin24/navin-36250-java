package test;

public class HourlyEmployee extends Employee{
	private double hourlyWage;
	private double hoursWorked;
	
	public HourlyEmployee() {
		this.hourlyWage = 0;
		this.hoursWorked = 0;
	}

	public HourlyEmployee(double hourlyWage, double hoursWorked) {
		super();
		this.hourlyWage = hourlyWage;
		this.hoursWorked = hoursWorked;
	}

	public double getHourlyWage() {
		return hourlyWage;
	}

	public void setHourlyWage(double hourlyWage) {
		this.hourlyWage = hourlyWage;
	}

	public double getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(double hoursWorked) {
		this.hoursWorked = hoursWorked;
	}
	
	public void printPayrollHourly( double salary ) {
		System.out.println();
		System.out.println("Name : "+this.getFirstName()+" "+this.getLastName());
		System.out.println("Tyape : Hourly");
		System.out.println("Hourly wage : "+this.getHourlyWage());
		System.out.println("Hour worked : "+this.getHoursWorked());
		System.out.println("Salary : "+salary);
		System.out.println();
		
	}
	public void payrollCalculation() {
		if( this.hoursWorked <= 40) {
			double salary = this.hourlyWage * this.hoursWorked;
			this.printPayrollHourly( salary );
		}
	}
}
