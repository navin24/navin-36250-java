package test;

public class BasePlusCommisionEmployee extends CommissionRateEmployee {
	private double baseSalary;
	
	public BasePlusCommisionEmployee() {
		this.baseSalary = 0;
	}

	public BasePlusCommisionEmployee(double baseSalary) {
		super();
		this.baseSalary = baseSalary;
	}

	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}
	
	public void printPayrollBase( double salary ) {
		System.out.println();
		System.out.println("Name : "+this.getFirstName()+" "+this.getLastName());
		System.out.println(("Type : Base + commission"));
		System.out.println("Gross sales : "+this.getGrossSales());
		System.out.println("Commission rate : "+this.getCommissionRate());
		System.out.println("Base salary : "+this.getBaseSalary());
		System.out.println("Salary : "+salary);
		System.out.println();
	}
	public void payrollCalculation() {
		double salary = ( ( this.commissionRate * this.grossSales ) + this.baseSalary );
		this.printPayrollBase( salary );
	}
}