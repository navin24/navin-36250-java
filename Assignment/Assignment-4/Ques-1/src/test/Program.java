package test;
import java.util.Scanner;

class Address{
	Scanner sc = new Scanner(System.in);
	private String add;
	public Address(){
	
	}
	public Address(String add) {
		this.add=add;
	}
	public void acceptAdd() {
		System.out.print("Enter Address	:");
		this.add=sc.nextLine();
	}
	public void printAdd() {
		System.out.println("Your Address	:"+this.add);
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	
};
class Date{
	Scanner sc = new Scanner(System.in);
    private int day;
    private int month;
    private int year;
    public Date(  ){
        this.month = 0;
        this.day = 0;
        this.year = 0;
    }
    public Date( int month, int day, int year ){
        this.month = month;
        this.day = day;
        this.year = year;
    }
    public void acceptDate( ){
        System.out.print("Month   :   ");
        this.month = sc.nextInt();
        System.out.print("Day   :   ");
        this.day = sc.nextInt();
        System.out.print("Year   :   ");
        this.year = sc.nextInt();
    }
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void displayDate( ){
        System.out.println("Date	:"+this.month+" / "+this.day+" / "+this.year);
    }
};

public class Program{
   public static void main(String[] args) {
       Date dt1 = new Date( );  //Instantiation
       Address a = new Address();
       dt1.acceptDate();
       a.acceptAdd();
       dt1.displayDate();      
       a.printAdd();       
   }
}
/*package test;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}*/
