import java.io.Console;

class q3_assign{
    public static void main(String arsgs[])
    {
        Console c = System.console();
        System.out.print("Enter Miles driven per day: ");
        int miles=Integer.parseInt(c.readLine());
        System.out.print("Enter cost per litre of petrol: ");
        int per_litre=Integer.parseInt(c.readLine());
        System.out.print("Enter average miles per litre: ");
        int avg_miles=Integer.parseInt(c.readLine());
        System.out.print("Enter parking fees per day: ");
        int pfees=Integer.parseInt(c.readLine());
        System.out.print("Enter sum of all tolls prices of each day: ");
        int tolls=Integer.parseInt(c.readLine());

        int dailyDrivingCost = (miles / avg_miles) * per_litre + pfees + tolls;

        System.out.println("Total Daily cost: "+dailyDrivingCost);

    }
}