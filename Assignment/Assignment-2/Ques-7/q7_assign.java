import java.io.Console;
class Employee{
    private String first_name;
    private String last_name;
    private float sal;
    private float raise;

    public Employee()
    {
        this.first_name=new String();
        this.last_name=new String();
        this.sal=0.0f;
    }
    public void setFirstName(String first_name)
    {
        this.first_name=first_name;
    }
    public void setLastName(String last_name)
    {
        this.last_name=last_name;
    }
    public void setSalary(float sal)
    {
        if(sal<=8000)
        {
            System.out.println("Enter a valid salary");
        }
        else
            this.sal=sal;
    }
    
    public String getFirstName()
    {
        return this.first_name;
    }
    public String getLastName()
    {
        return this.last_name;
    }
    public float getSalary()
    {
        return this.sal;
    }
    void acceptRecord()
    {
        Console c=System.console();
        System.out.print("First Name    :");
        this.first_name=c.readLine();
        System.out.print("Last Name    :");
        this.last_name=c.readLine();
        System.out.print("Salary    :");
        this.sal=Float.parseFloat(c.readLine());
    }
    void printRecord()
    {
        System.out.println("First Name    :"+this.first_name);
        System.out.println("Last Name    :"+this.last_name);
        System.out.println("Salary    :"+12*this.sal);
    }
    public void printRaise()
    {
        System.out.println("Annual Salary After 10% raise: "+(12*((this.sal*1.1)+this.sal)));
    }
};

    class EmployeeTest{

        public static void main(String args[])
        {
            Employee e1=new Employee();
            Employee e2=new Employee();
            System.out.println("Details of first Employee:");
		    e1.acceptRecord();
            e1.printRecord();  
            e1.printRaise();

            System.out.println("Details of second Employee:");
            e2.acceptRecord();
            e2.printRecord();
            e2.printRaise();  
        
        }

    }
