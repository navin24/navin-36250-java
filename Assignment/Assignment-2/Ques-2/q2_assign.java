import java.io.Console;

class q2_assign{
    public static void main(String arsgs[])
    {
        float cal=0f;
        Console c = System.console();
        System.out.print("Enter your weight in Kilograms: ");
        float kilo=Float.parseFloat(c.readLine());
        System.out.print("Enter your height in metres: ");
        float meter=Float.parseFloat(c.readLine());
        cal= (float)kilo/(meter*meter);
        if(cal<18.5)
        {
            System.out.println("You're Underweight");
        }
        else if(cal>18.5 && cal<24.9)
        {
            System.out.println("You're Normal");
        }
        else if(cal>25 && cal<29.9)
        {
            System.out.println("You're Overweight");
        }
        else
        {
            System.out.println("You're Obese");
        }
    }
}
        