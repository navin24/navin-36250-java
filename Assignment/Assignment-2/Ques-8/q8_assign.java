import java.io.Console;

class Date{
    private int day;
    private int month;
    private int year;
    public Date(  ){
        this.month = 0;
        this.day = 0;
        this.year = 0;
    }
    public Date( int month, int day, int year ){
        this.month = month;
        this.day = day;
        this.year = year;
    }
    public void acceptDate( ){
        Console console =   System.console();
        System.out.print("Month   :   ");
        this.month = Integer.parseInt(console.readLine());
        System.out.print("Day   :   ");
        this.day = Integer.parseInt(console.readLine());
        System.out.print("Year   :   ");
        this.year = Integer.parseInt(console.readLine());
    }
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void displayDate( ){
        System.out.println(this.month+" / "+this.day+" / "+this.year);
    }
}
class DateTest{
   public static void main(String[] args) {
       Date dt1 = new Date( );  //Instantiation
       dt1.acceptDate();
       dt1.displayDate();
   }
}