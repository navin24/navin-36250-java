import java.io.Console;

class q1_assign{
    public static void main(String arsgs[])
    {
        int ch;
        Console c = System.console();    
        System.out.print("Enter 1st number: ");
        int num1=Integer.parseInt(c.readLine());
        System.out.print("Enter 2nd number: ");
        int num2=Integer.parseInt(c.readLine());
        
        do
        {
            System.out.println("\n0.Exit\n 1.Addition\n 2.Subtraction\n 3.Multiplication\n 4.Division\n");
            System.out.println("Enter Your Choice");
            ch=Integer.parseInt(c.readLine());
            
            switch(ch)
            {
                case 0:
                    break;
                case 1:
                    System.out.println("Addition of two numbers: "+(num1+num2));
                    break;
                case 2:
                    System.out.println("Subtraction of two numbers: "+(num1-num2));
                    break;
                case 3:
                    System.out.println("Multiplication of two numbers: "+(num1*num2));
                    break;
                case 4:
                    System.out.println("Division of two numbers: "+((float)num1/num2));
                    break;
                default:
                    System.out.println("Invalid Option");
                    break;
            }
        }while(ch!=0);        
    }
}