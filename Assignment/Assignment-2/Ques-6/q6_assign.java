import java.io.Console;

class Invoice
{
    private String part_number;
    private String part_desc;
    private int item_quantity;
    private double price_item;
    public Invoice( )
    {
        this.part_number = new String();
        this.part_desc = new String();
        this.item_quantity = 0;
        this.price_item=0.0;
    }
    public Invoice( String part_number,String part_desc, int item_quantity,double price_item )
    {
        this.part_number = part_number;
        this.part_desc = part_desc;
        this.item_quantity = item_quantity;
        this.price_item=price_item;        
    }
    public void invoiceEntry( )
    {
        Console console =   System.console();
        System.out.print("Part Number   :   ");
        this.part_number = console.readLine();
        System.out.print("Part Description   :   ");
        this.part_desc = console.readLine();
        System.out.print("Number of items(Quantity)   :   ");
        this.item_quantity = Integer.parseInt(console.readLine());
        System.out.print("Items Price   :   ");
        this.price_item = Double.parseDouble(console.readLine());
    }
    public String getPartNumber() {
        return part_number;
    }
    public void setDay(String part_number) {
        this.part_number = part_number;
    }
    public String getPartDescription() {
        return part_desc;
    }
    public void setPartDescription(String part_desc) {
        this.part_desc = part_desc;
    }
    public int getItemNumber() {
        return item_quantity;
    }
    public void setItemNumber(int item_quantity) 
    {
	if(item_quantity<0)
		this.item_quantity=0;
	else
	        this.item_quantity=item_quantity;
    }
    public double getPrice()
    {
        return price_item;
    }
    public void setPrice(double price_item)
    {
        this.price_item=price_item;
    }
    public void printInvoice( )
    {
        System.out.println("Part Number :   "+this.part_number);
        System.out.println("Part description    :   "+this.part_desc);
        System.out.println("Quantity of Item    :   "+this.item_quantity);
        System.out.println("Item Price  :   "+this.price_item);
        getInvoiceAmount(this.item_quantity,this.price_item);
    }
    public void getInvoiceAmount(double item_quantity,double price_item)
    {
        System.out.println("Total Price of Item(Related to Quantity) :   "+this.item_quantity*this.price_item);
    }
}
class InvoiceTest{
   public static void main(String[] args)
    {
       Invoice i1 = new Invoice( );  
       i1.invoiceEntry();
       System.out.println("\n");
       i1.printInvoice();
   }
}
