package test;
//import java.util.HashMap;
import java.util.Set;

public class ServiceStation {
	private String name;
	private Set<Customer> customer;
	public ServiceStation() {}
	public ServiceStation(String name, Set<Customer> customer) {
		this.name = name;
		this.customer = customer;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Customer> getCustomer() {
		return customer;
	}
	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}
	
	public void newCustomer()
	{
		Customer c = new Customer();
		c.input();
		
		this.customer.add(c);
	}
	public Customer findCustomer(String name)
	{
		if(customer!=null)
		{
			for(Customer cust :this.customer)
			{
				if(cust.getName().equalsIgnoreCase(name))
					return cust;
			}
		}
		return null;
	}
}
