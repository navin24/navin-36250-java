package test;

public class ServiceRequest {
	private String custName;
	private String vehNumber;
	public ServiceRequest(String custName, String vehNumber) {
		this.custName = custName;
		this.vehNumber = vehNumber;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getVehNumber() {
		return vehNumber;
	}
	public void setVehNumber(String vehNumber) {
		this.vehNumber = vehNumber;
	}
	
	
	
}
