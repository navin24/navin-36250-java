package test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet; 

public class Program {

	public static String getLine() {
		try {
		BufferedReader read = new BufferedReader( new InputStreamReader(System.in)); 

		return read.readLine();
		}
		catch(IOException e) {
		throw new RuntimeException(e);
		}
		} 
	public static int menuList()
	{
		System.out.println("0:Exit\n1:Register New Customer\n2:Servicing Request\n3:Today's Business\n");
		System.out.print("Enter your Choice	:");
		return Integer.parseInt(Program.getLine());
	}
	public static void main(String[] args) {
		int mainCh=0;
		ServiceStation st = new ServiceStation();
		st.setCustomer(new HashSet<>());
		
		while((mainCh=menuList())!=0)
		{
			switch(mainCh)
			{
				case 0:
					break;
				case 1:
					System.out.println("Register New Customer");
					st.newCustomer();
					break;
				case 2:
					System.out.println("Servicing Request");
					//Service s = new Service();
					break;
				case 3:
					System.out.println("Business");
					System.out.print("Enter name:");
					String str = Program.getLine();
					Customer c = st.findCustomer(str);
					System.out.println(c.toString());
					break;
				default:
					System.out.println("Invalid Choice");
					break;
			}
		}

	}

}
