package test;

import java.util.LinkedList;
import java.util.List;

public class Service {
	protected String desc;
	public Service()
	{
		
	}

	public Service(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void input() {		
	}
	public void display()
	{
		//Oil o = new Oil();
		List<Part> list = new LinkedList<>();
		int choice;
		do
		{
			System.out.println("0:Servicing Done\n1:Oil Change\n2:Maintenance/Repairing\n");
			System.out.print("Enter your choice	:	");
			choice = Integer.parseInt(Program.getLine());
			switch(choice)
			{
			case 0:
				break;
			case 1:
				System.out.println("Oil");
				break;
			case 2:
				Maintenance m = new Maintenance();
				m.input();
				m.display(list);
				break;
			}
		}while(choice!=0);
	}
}
