package test;

public class Vehicle {
	private String company;
	private String model;
	private String number;
	
	public Vehicle(String company, String model, String number) {
		this.company = company;
		this.model = model;
		this.number = number;
	}
	public void input()
	{
		System.out.print("Enter Vehile Company	:	");
		this.company = Program.getLine();
		System.out.print("Enter Vehile Model	:	");
		this.model = Program.getLine();
		System.out.print("Enter Vehile Number	:	");
		this.number = Program.getLine();
		System.out.println();
	}
	public Vehicle() {
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String toString()
	{
		return String.format("-15%s -15%s -15%s",this.company,this.model,this.number);
	}

}
