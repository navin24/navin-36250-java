package test;

import java.util.Map;

public class Customer {
	private String address;
	private String mobile;
	private String name;
	private Map<String,Vehicle> vehicle;
	
	public Customer() {}
	public Customer(String address, String mobile, String name) {
		this.address = address;
		this.mobile = mobile;
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, Vehicle> getVehicle() {
		return vehicle;
	}
	public void setVehicle(Map<String, Vehicle> vehicle) {
		this.vehicle = vehicle;
	}
	public void input()
	{
		System.out.print("Name	:	");
		this.name = Program.getLine();
		System.out.print("Mobile Number	:	");
		this.mobile = Program.getLine();
		System.out.print("Enter Address	:	");
		this.address = Program.getLine();
		System.out.println();
	}
	public void newVehicle()
	{
		Vehicle v = new Vehicle();
		v.input();
		
		this.vehicle.put(v.getNumber(),v);
	}
	@Override
	public String toString() {
		return String.format("%-10s @ %-10s", this.name,this.address);
	}
}
