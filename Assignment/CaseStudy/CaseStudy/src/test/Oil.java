package test;

public class Oil extends Service {
	private double oilCost;
	public Oil() {}

	public Oil(String desc , double oilCost){
		super(desc);
		this.oilCost = oilCost;
	}

	public double getOilCost() {
		return oilCost;
	}

	public void setOilCost(double oilCost) {
		this.oilCost = oilCost;
	}
	public void input()
	{
		System.out.println("Enter Oil Details");
		System.out.print("Description	:	");
		this.desc = Program.getLine();
		System.out.print("Enter Price	:	");
		this.oilCost = Double.parseDouble(Program.getLine());
		System.out.println();
		
	}
	
	
}
