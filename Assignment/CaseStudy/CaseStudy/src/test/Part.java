package test;

public class Part {
	private String partDesc;
	private double partPrice;
	public Part() {}
	public Part(String partDesc, double partPrice) {
		this.partDesc = partDesc;
		this.partPrice = partPrice;
	}
	public String getpartDesc() {
		return partDesc;
	}
	public void setpartDesc(String partDesc) {
		this.partDesc = partDesc;
	}
	public double getPartPrice() {
		return partPrice;
	}
	public void setPartPrice(double partPrice) {
		this.partPrice = partPrice;
	}
	
	public void inputPart()
	{
		System.out.println("Enter Part Details");
		System.out.print("Part Description	:	");
		this.partDesc = Program.getLine();
		System.out.println();
		
		System.out.print("Part Rate	:	");
		this.partPrice = Double.parseDouble(Program.getLine());
		System.out.println();		
	}
	public String toString()
	{
		return String.format("%s %u",partDesc,partPrice);
	}
}
