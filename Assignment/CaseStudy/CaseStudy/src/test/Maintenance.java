package test;
import java.util.Iterator;
import java.util.List;
public class Maintenance extends Service{
	private double labourCharges;
	private List<Part> partList;

	public Maintenance(String desc , double labourCharges,List<Part> partList) {
		super(desc);
		this.labourCharges = labourCharges;
		this.partList= partList;
	}

	public Maintenance(){
	}

	public double getLabourCharges() {
		return labourCharges;
	}

	public void setLabourCharges(double labourCharges) {
		this.labourCharges = labourCharges;
	}
	public void addPart()
	{
		Part p = new Part();
		p.inputPart();
		this.partList.add(p);
	}
	@Override
	public void input()
	{
		System.out.println("Enter Maintenance/Repairing Details");
		System.out.print("Description	:	");
		this.desc = Program.getLine();
	}
	public void display(List<Part> list)
	{
		int mChoice=0;
		this.partList = list;
		do
		{
			System.out.println("0:Exit\n1:AddPart\n");
			System.out.print("Enter Your Choice	:");
			mChoice = Integer.parseInt(Program.getLine());
			switch(mChoice)
			{
				case 0:
					break;
				case 1:
					this.addPart();
					break;
			}
		}while(mChoice!=0);
		System.out.print("Enter Labour Charges	:	");
		this.labourCharges = Double.parseDouble(Program.getLine());
	}
//	public void print()
//	{
//		System.out.println(this.labourCharges);
//		
//		Part p = null;
//		Iterator<Part> itr = this.partList.iterator();
//		while(itr.hasNext())
//		{
//			p = itr.next();
//			System.out.println(p.getpartDesc()+ " " +p.getPartPrice());
//		}
//		
	//}
	@Override
	public String toString() {
		return String.format("%10.2lf",this.labourCharges);
	}
}
