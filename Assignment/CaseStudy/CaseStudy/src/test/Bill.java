package test;

public class Bill {
	private double amount;
	private double paidAmount;
	private ServiceRequest serReq;
	public Bill(double amount, double paidAmount, ServiceRequest serReq) {
		this.amount = amount;
		this.paidAmount = paidAmount;
		this.serReq = serReq;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public ServiceRequest getSerReq() {
		return serReq;
	}
	public void setSerReq(ServiceRequest serReq) {
		this.serReq = serReq;
	}
	
	
	
	
	

}
