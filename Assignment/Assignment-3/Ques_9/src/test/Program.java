package test;

public class Program {
	enum TrafficLight{
		RED(60), GREEN(60), YELLOW(30);
		private int value;
		private TrafficLight(int value)
		{
			this.value = value;
		}
	}
	public static void main(String[] args) {
		for(TrafficLight t : TrafficLight.values())
			System.out.println(t+" "+t.value+" seconds");
	}
}
