package test;

import java.util.Scanner;

class Queue{
	int[] queue;
	int top;
	int size;
	
	public Queue( int s ) {
		this.size = s;
		this.top = -1;
		this.queue = new int[s];
	}
	public void printQueue( int i ) {
		System.out.print("Queue : ");
		for(int j = 0; j <= i; j++) {
			System.out.print(this.queue[j]+" ");
		}
		System.out.println();
	}
}

class Program{
	private static Scanner sc = new Scanner(System.in);

	private static boolean queueFullCheck(Queue que) {
		boolean check = false;
		if( que.top == (que.size - 1) ) {
			check = true;
			return check;
		}
		return false;
	}

	private static boolean queueEmptyCheck(Queue que) {
		boolean check = false;
		if( que.top < 0) {
			check = true;
			return check;
		}
		return false;
	}


	private static void queuePeek(Queue que) {
		if( que.top >= 0 ) {
			System.out.println("queue peek : "+que.queue[que.top]);
		}
		else
			System.out.println("Queue is empty");
		
		que.printQueue(que.top);
	}
	
	private static void queueUnload(Queue que) {
		if(que.top >= 0) {
			System.out.println("unload : "+que.queue[0]);
			for(int i = 1; i <= que.top; i++) {
				que.queue[ i - 1 ] = que.queue[ i ];
			}
			que.top--;
		}
		else
			System.out.println("Queue is empty");
		que.printQueue(que.top);
	}
	
	private static void queueLoad(Queue que) {
		if( que.top < ( que.size - 1 ) ) {
			System.out.println("Enetr element to load : ");
			que.queue[++(que.top)] = sc.nextInt();
			System.out.println("load : "+que.queue[que.top]);
		}
		else {
			System.out.println("Queue is full");
		}
		que.printQueue(que.top);
	}
	
	private static int menuList() {
		System.out.printf("\n0.Exit\n1.Load\n2.unload\n3.Peek\n4.Check empty\n5.Check full\n6.Size\nEnetr your choice : \n");
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		System.out.println("Queue Test");
		System.out.println();
		int size = 0, choice = 0;
		System.out.println("Enetr maximum size of Queue : ");
		size = sc.nextInt();
		
		Queue que  = new Queue(size);
		
		while((choice = Program.menuList()) != 0) {
			switch( choice ) {
				case 0:
					break;
				
				case 1:
					Program.queueLoad(que);
					break;
				
				case 2:
					Program.queueUnload(que);
					break;
				
				case 3:
					Program.queuePeek(que);
					break;
				
				case 4:
					boolean empty = false;
					empty = Program.queueEmptyCheck(que);
					if(empty) {
						System.out.println("Queue empty status : true");
					}
					else {
						System.out.println("Queue empty status : false");
					}
					que.printQueue(que.top);
					break;
				
				case 5:
					boolean full = false;
					full = Program.queueFullCheck(que);
					if(full) {
						System.out.println("Queue full status : true");
					}
					else {
						System.out.println("Queue full status : false");
					}
					que.printQueue(que.top);
					break;
				
				case 6:
					System.out.println("Size : "+que.size);
					break;
				
				default :
					System.out.println("Wrong choice");
					break;
			}
		}
	}


}
