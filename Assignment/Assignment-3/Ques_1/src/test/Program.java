package test;

import java.util.Scanner;

public class Program {
	private static Scanner sc=new Scanner(System.in);
	public static void acceptRecord(int[][] arr) {
		if(arr!=null)
		{
			for(int row=0;row<arr.length;row++)
			{
				for(int col=0;col<arr[row].length;col++) 
				{
					System.out.print("Enter element:");
					arr[row][col]=sc.nextInt();
				}
			}
		}

	}
	public static void printRecord(int[][] arr) 
	{
		for(int[] ref : arr)
		{
			for(int element : ref)
				System.out.print(element+" ");
			System.out.println();
		}
	}
	
	public static void main(String ags[])
	{
		
		int[][] arr = new int[4][];
		arr[0]=new int[4];
		arr[1]=new int[3];
		arr[2]=new int[2];
		arr[3]=new int[1];
		Program.acceptRecord(arr);
		Program.printRecord(arr);		
		
	}
}
