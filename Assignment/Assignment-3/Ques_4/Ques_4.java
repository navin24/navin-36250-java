package test;

import java.util.Scanner;

class Stack{
	int[] stack;
	int top;
	int size;
	
	public Stack( int s ) {
		this.size = s;
		this.top = -1;
		this.stack = new int[s];
	}
	public void printStack( int i ) {
		System.out.print("Stack : ");
		for(int j = 0; j <= i; j++) {
			System.out.print(this.stack[j]+" ");
		}
		System.out.println();
	}
}

class Program{
	private static Scanner sc = new Scanner(System.in);

	private static boolean stackFullCheck(Stack st) {
		boolean check = false;
		if( st.top == (st.size - 1) ) {
			check = true;
			return check;
		}
		return false;
	}

	private static boolean stackEmptyCheck(Stack st) {
		boolean check = false;
		if( st.top < 0) {
			check = true;
			return check;
		}
		return false;
	}


	private static void stackPeek(Stack st) {
		if( st.top >= 0 ) {
			System.out.println("stack peek : "+st.stack[st.top]);
		}
		else
			System.out.println("Stack is empty");
		
		st.printStack(st.top);
	}
	
	private static void stackPop(Stack st) {
		if(st.top >= 0) {
			System.out.println("Pop : "+st.stack[st.top--]);
		}
		else
			System.out.println("Stack is empty");
		st.printStack(st.top);
	}
	
	private static void stackPush(Stack st) {
		if( st.top < ( st.size - 1 ) ) {
			System.out.println("Enetr element to push : ");
			st.stack[++(st.top)] = sc.nextInt();
			System.out.println("Push : "+st.stack[st.top]);
		}
		else {
			System.out.println("Stack is full");
		}
		st.printStack(st.top);
	}
	
	private static int menuList() {
		System.out.printf("\n0.Exit\n1.Push\n2.Pop\n3.Peek\n4.Check empty\n5.Check full\n6.Size\nEnetr your choice : \n");
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		System.out.println("Stack Test");
		System.out.println();
		int size = 0, choice = 0;
		System.out.println("Enetr maximum size of stack : ");
		size = sc.nextInt();
		
		Stack st  = new Stack(size);
		
		while((choice = Program.menuList()) != 0) {
			switch( choice ) {
				case 0:
					break;
				
				case 1:
					Program.stackPush(st);
					break;
				
				case 2:
					Program.stackPop(st);
					break;
				
				case 3:
					Program.stackPeek(st);
					break;
				
				case 4:
					boolean empty = false;
					empty = Program.stackEmptyCheck(st);
					if(empty) {
						System.out.println("Stack empty status : true");
					}
					else {
						System.out.println("Stack empty status : false");
					}
					st.printStack(st.top);
					break;
				
				case 5:
					boolean full = false;
					full = Program.stackFullCheck(st);
					if(full) {
						System.out.println("Stack full status : true");
					}
					else {
						System.out.println("Stack full status : false");
					}
					st.printStack(st.top);
					break;
				
				case 6:
					System.out.println("Size : "+(st.size));
					break;
				
				default :
					System.out.println("Wrong choice");
					break;
			}
		}
	}


}
