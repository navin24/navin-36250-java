package test;

public class Saving {
	static double rate;
	double balance;
	static {
		rate = 4;
	}
	public Saving() {
		this.balance = 0;
	}
	public Saving(double balance) {
		this.balance = balance;
	}
	public static double getRate() {
		return rate;
	}
	public static void setRate(double rate) {
		Saving.rate = rate;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		if(balance >= 0)
			this.balance = balance;
	}
	public static void modifyInterestRate(double newRate) {
		Saving.rate = newRate;
	}
	public void calculateMonthlyInterest() {
		this.balance = this.balance + ( ( (this.balance * Saving.rate) / 100 ) / 12 ) ;
	}
}
