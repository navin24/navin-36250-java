package test;

import java.util.Scanner;

class Program{
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		Saving saver1 = new Saving(2000.0);
		Saving saver2 = new Saving(3000.0);
		double rate = 0.0;
		
		System.out.println("Enter new rate : ");
		rate = sc.nextDouble();
		
		Saving.modifyInterestRate(rate);
		
		for(int i = 1; i <= 12; i++ ) {
			saver1.calculateMonthlyInterest();
			saver2.calculateMonthlyInterest();
		}
		
		System.out.println("Saver1 balance : "+saver1.balance);
		System.out.println("Saver2 balance : "+saver2.balance);
		
		System.out.println("Enter new rate : ");
		rate = sc.nextDouble();
		
		Saving.modifyInterestRate(rate);
		
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		
		System.out.println("Saver1 balance : "+saver1.balance);
		System.out.println("Saver2 balance : "+saver2.balance);
		
	}	
		
	
}

	
