package test;

import java.util.Scanner;

class Program{
	static Scanner sc = new Scanner(System.in);

	private static int reduceForm(int num, int deno) {
		int i = 0;
		num = Math.abs(num);
		deno = Math.abs(deno);
		if( num < deno ) {
			for( i = num; i > 1; i--) {
				if( ((num % i) == 0 ) && ((deno % i) == 0 ) ) {
					return i;
				}
			}
		}
		else if ( deno < num ) {
			for (i = deno; i > 1; i--) {
				if( ((num % i) == 0 ) && ((deno % i) == 0 ) ) {
					return i;
				}
			}
		}
		else if( num == deno ) {
			return num;
		}
		
		return 1;
	}   
	
	public static void main(String[] args) {
		int num = 0, deno = 0, hcf = 0;
		Rational num1 = null;
		Rational num2 = null;
		Rational res = null;
		
		System.out.println("Enter 1st number numerator : ");
		num = sc.nextInt();
		System.out.println("Enter 1st number denominator : ");
		deno = sc.nextInt();
		hcf = Program.reduceForm(num,deno);
		num1 = new Rational((num/hcf),(deno/hcf));
		
		System.out.println("Enter 2nd number numerator : ");
		num = sc.nextInt();
		System.out.println("Enter 2nd number denominator : ");
		deno = sc.nextInt();
		hcf = Program.reduceForm(num,deno);
		num2 = new Rational((num/hcf),(deno/hcf));
		
		res = Rational.add(num1, num2);
		hcf = Program.reduceForm(res.getNumerator(), res.getDenominator());
		res.setNumerator( (res.getNumerator() / hcf) );
		res.setDenominator((res.getDenominator() / hcf) );
		System.out.print("Addition : ");
		res.printRecord();
		System.out.println();
		
		res = Rational.sub(num1, num2);
		hcf = Program.reduceForm(res.getNumerator(), res.getDenominator());
		res.setNumerator( (res.getNumerator() / hcf) );
		res.setDenominator((res.getDenominator() / hcf) );
		System.out.print("Subtractin : ");
		res.printRecord();
		System.out.println();
		
		res = Rational.mul(num1, num2);
		hcf = Program.reduceForm(res.getNumerator(), res.getDenominator());
		res.setNumerator( (res.getNumerator() / hcf) );
		res.setDenominator((res.getDenominator() / hcf) );
		System.out.print("Multiplication : ");
		res.printRecord();
		System.out.println();
		
		res = Rational.div(num1, num2);
		hcf = Program.reduceForm(res.getNumerator(), res.getDenominator());
		res.setNumerator( (res.getNumerator() / hcf) );
		res.setDenominator((res.getDenominator() / hcf) );
		System.out.print("Division : ");
		res.printRecord();
		System.out.println();
		
	}

}