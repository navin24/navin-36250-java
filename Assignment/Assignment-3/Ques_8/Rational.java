package test;

class Rational {
	private int numerator;
	private int denominator;
	
	public Rational(){
		this.numerator = 1;
		this.denominator = 1;
	}
	public Rational(int num, int deno) {
		this.numerator = num;
		this.denominator = deno;
	}
	public int getNumerator() {
		return numerator;
	}
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	public int getDenominator() {
		return denominator;
	}
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	public static Rational add(Rational num1, Rational num2) {
		Rational res = new Rational();
		res.numerator =  ( ( num2.denominator * num1.numerator ) + ( num1.denominator * num2.numerator) );
		res.denominator = ( num1.denominator * num2.denominator );
		
		return res;
	}
	public static Rational sub(Rational num1, Rational num2) {
		Rational res = new Rational();
		res.numerator =  ( ( num2.denominator * num1.numerator ) - ( num1.denominator * num2.numerator) );
		res.denominator = ( num1.denominator * num2.denominator );
		
		return res;
	}
	public static Rational mul(Rational num1, Rational num2) {
		Rational res = new Rational();
		res.numerator =  ( num2.numerator * num1.numerator );
		res.denominator = ( num1.denominator * num2.denominator );
		
		return res;
	}
	public static Rational div(Rational num1, Rational num2) {
		Rational res = new Rational();
		res.numerator =  ( num2.denominator * num1.numerator );
		res.denominator = ( num1.denominator * num2.numerator );
		
		return res;
	}
	public void printRecord() {
		System.out.println(this.numerator+" / "+this.denominator);
	}
	
}