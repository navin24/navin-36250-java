package test;

import java.util.Scanner;

public class Program {
	
	private static Scanner sc = new Scanner(System.in);
	
	private static boolean[] seat = new boolean[10];
	
	private static int full;
	
	static {
		full = 0;
	}
	
	private static int menuList() {
		System.out.printf("0.Exit\n1.First class\n2.Economy class\n\nEnetr your choice : \n");
		return sc.nextInt();
	}
	
	private static int checkSeatAvailability( int start, int end, String str ) {
		int i = 0;
		for ( i = start; i <= end; i++) {
			if ( !(seat[ i - 1 ] ) ) {
				return (i - 1);
			}
		}
		if ( str == "Economy Class") {
			return 10;
		}
		return 5;
	}

	private static void bookSeat(int book, int index, String className) {
		System.out.println();
		System.out.println("Class   : "+className);
		System.out.print("Seat no : ");
		for ( int i = 0; i < book; i++) {
			seat[index++] = true;
			System.out.print(index+" ");
			if( index == 5 || index == 10) {
				full += 1;
			}
		}
		System.out.println();
		System.out.println("Booking confirmed");
	}
	
	private static void firstClass() {
		if( full < 2) {
			int index = Program.checkSeatAvailability( 1 , 5, "First Class" );
			int book = 0;
		
			int available = 5 - index;
			if ( available == 0) {
				int sw = 0;
				System.out.println();
				System.out.println("All seat of first class are full");
				System.out.println("Enter 1 if you want to book seat from economy class else enter 0 to go to main menu : ");
				sw = sc.nextInt();
				if ( sw == 1 ) {
					Program.economyClass();
				}
				else {
					System.out.println();
					System.out.println("Next flight leaves in 3 hours.");
					System.out.println();
				}
			}
			else {
				System.out.println();
				System.out.println("In First class total available seats : "+available);
				System.out.println("Enter total number of seats to book : ");
				book = sc.nextInt();
				if ( book > available ) {
					System.out.println();
					System.out.println( book+" : Seat not available   "+"Total seat available : "+available);
				}
				else {
					Program.bookSeat( book, index, "First Class" );
				}
			}
		}
		else {
			System.out.println("All seat of first class and economy class are full");
		}
	}

	private static void economyClass() {
		if ( full < 2) {
			int index = Program.checkSeatAvailability(6, 10, "Economy Class");
			int book = 0;
		
			int available = 5 - ( index - 5 );
			if ( available == 0) {
				int sw = 0;
				System.out.println();
				System.out.println("All seat of Economy class are full");
				System.out.println("Enter 1 if you want to book seat from first class else enter 0 to go to main menu : ");
				sw = sc.nextInt();
				if ( sw == 1 ) {
					Program.firstClass();
				}
				else {
					System.out.println();
					System.out.println("Next flight leaves in 3 hours.");
					System.out.println();
				}
			}
			else {
				System.out.println();
				System.out.println("In Economy Class total available seats : "+available);
				System.out.println("Enter total number of seats to book");
				book = sc.nextInt();
				if ( book > available ) {
					System.out.println();
					System.out.println( book+"  : Seat not available    "+"Total seat available : "+available);
				}
				else {
					Program.bookSeat(book, index, "Economy Class" );
				}
			}
		}
		else {
			System.out.println("All seat of first class and economy class are full");
		}
	}

	public static void main(String[] args) {
		int choice = 0;
		
		do {
			System.out.println();
			choice = Program.menuList();
			
			switch( choice ) {
				case 0:
					break;
				
				case 1:
					Program.firstClass();
					break;
				
				case 2:
					Program.economyClass();
					break;
				
				default :
					System.out.println("Wrong choice");
					System.out.println();
			}
		}while( choice != 0);
	}

}
