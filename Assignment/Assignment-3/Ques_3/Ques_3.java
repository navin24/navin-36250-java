package test;

class Program{
	private static int sum;
	static {
		sum = 0;
	}
	private static void colAdd(int[][] arr) {
		int row = 0, col = 0;
		for(col = 0; col < (arr[row].length - 1); col++) {
			sum = 0;
			for(row = 0; row < ( arr.length - 1); row++) {
				sum += arr[row][col];
			}
			arr[row][col] = sum;
		}
	}
	private static void rowAdd(int[][] arr) {
		int row = 0, col = 0;
		for(row = 0; row < (arr.length - 1); row++) {
			sum = 0;
			for(col = 0; col < ( arr[row].length - 1); col++) {
				sum += arr[row][col];
			}
			arr[row][col] = sum;
		}
	}
	public static void printArr( int[][] arr ) {
		int i = 0;
		System.out.printf("%-15s%-15s%-15s%-15s%-15s%-15s\n","Producct Name","Person 1","Person 2", "Person 3","Person 4", "Product total");
		for(int[] ref : arr) {
			if( i != 5)
				System.out.printf("%-15s", "Product"+(++i));
			else 
				System.out.printf("%-15s","Total sell");
			for(int element : ref) {
				System.out.printf("%-15d",element);
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		int[][] arr = new int[][] { {10,20,30,40,0}, {50,60,70,80,0}, {90,100,110,120,0}, {130,140,150,160,0}, {180,190,200,210,0}, {0,0,0,0,0} };
		
		System.out.println("Init array : ");
		Program.printArr(arr);
		
		System.out.println();
		System.out.println();
		
		System.out.println("Array after row addition (Total product sell) : ");
		Program.rowAdd(arr);
		Program.printArr(arr);
		
		System.out.println();
		System.out.println();
		
		System.out.println("Array after column addition (Total column addition) : ");
		Program.colAdd(arr);
		Program.printArr(arr);
	}
	
}
